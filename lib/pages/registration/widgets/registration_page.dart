import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:ivoire_warren/main.dart';
import 'package:ivoire_warren/pages/home_page.dart';
import 'package:ivoire_warren/pages/login/login_page.dart';

class RegistrationPage extends StatelessWidget {
  RegistrationPage({Key? key}) : super(key: key);

  static const String idPage = 'registrationPage';

  final TextEditingController nameTextEditingController =
      TextEditingController();
  final TextEditingController emailTextEditingController =
      TextEditingController();
  final TextEditingController passwordTextEditingController =
      TextEditingController();
  final TextEditingController phoneTextEditingController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              const SizedBox(height: 20),
              const Image(
                image: AssetImage('assets/images/logo.png'),
                width: 390,
                height: 250,
                alignment: Alignment.center,
              ),
              // SizedBox(height: 15),
              const Text(
                'Register as a Rider',
                style: TextStyle(
                  fontFamily: 'Brand bold',
                  fontSize: 24,
                ),
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    const SizedBox(height: 2.0),
                    TextField(
                      controller: nameTextEditingController,
                      keyboardType: TextInputType.text,
                      style: const TextStyle(
                        fontSize: 14,
                      ),
                      decoration: const InputDecoration(
                        labelText: 'Name',
                        labelStyle: TextStyle(fontSize: 14),
                        hintStyle: TextStyle(
                          fontSize: 10,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    const SizedBox(height: 2.0),
                    TextField(
                      controller: emailTextEditingController,
                      keyboardType: TextInputType.emailAddress,
                      style: const TextStyle(fontSize: 14),
                      decoration: const InputDecoration(
                        labelText: 'Email',
                        labelStyle: TextStyle(fontSize: 14),
                        hintStyle: TextStyle(
                          fontSize: 10,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    const SizedBox(height: 2.0),
                    TextField(
                      controller: passwordTextEditingController,
                      obscureText: true,
                      style: const TextStyle(fontSize: 14),
                      decoration: const InputDecoration(
                        labelText: 'Password',
                        labelStyle: TextStyle(fontSize: 14),
                        hintStyle: TextStyle(
                          fontSize: 10,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    const SizedBox(height: 2.0),
                    TextField(
                      controller: phoneTextEditingController,
                      keyboardType: TextInputType.phone,
                      style: const TextStyle(fontSize: 14),
                      decoration: const InputDecoration(
                        labelText: 'Phone',
                        labelStyle: TextStyle(fontSize: 14),
                        hintStyle: TextStyle(
                          fontSize: 10,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    const SizedBox(height: 12.0),
                    TextButton(
                      child: const Text(
                        'Register',
                      ),
                      style: TextButton.styleFrom(
                        primary: Colors.white,
                        backgroundColor: Colors.amber,
                        minimumSize: const Size(350, 50),
                        side: const BorderSide(color: Colors.amber, width: 1),
                        elevation: 5,
                        shadowColor: Colors.yellow,
                        textStyle: const TextStyle(
                          fontSize: 18,
                          fontFamily: 'Brand bold',
                          color: Colors.white,
                        ),
                        // padding: const EdgeInsets.symmetric(
                        //   vertical: 20,
                        //   horizontal: 50,
                        // ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(24.0),
                        ),
                      ),
                      onPressed: () {
                        if (nameTextEditingController.text.length < 3) {
                          displayToastMessage(
                            context,
                            "Name must be at least 3 characters.",
                          );
                        } else if (!emailTextEditingController.text
                            .contains('@')) {
                          displayToastMessage(
                            context,
                            "Email address is not valid",
                          );
                        } else if (phoneTextEditingController.text.isEmpty) {
                          displayToastMessage(
                            context,
                            "Phone Number is mandatory",
                          );
                        } else if (passwordTextEditingController.text.length <
                            6) {
                          displayToastMessage(
                            context,
                            "Password must be at least 6 characters.",
                          );
                        } else {
                          registerNewUser(context);
                        }
                      },
                    ),
                  ],
                ),
              ),
              TextButton(
                child: const Text(
                  'Already have an Account? Login Here.',
                  style: TextStyle(color: Colors.black),
                ),
                onPressed: () {
                  Navigator.pushNamedAndRemoveUntil(
                      context, LoginPage.idPage, (route) => false);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  void registerNewUser(BuildContext context) async {
    final User firebaseUser = (await _firebaseAuth
            .createUserWithEmailAndPassword(
                email: emailTextEditingController.text,
                password: passwordTextEditingController.text)
            .catchError((onError) {
      displayToastMessage(context, 'Error : ${onError.toString()}');
    }))
        .user!;
    if (firebaseUser != null) {
      // on enregistre les infos de user dans la base de donnee firebase

      Map userDataMap = {
        "name": nameTextEditingController.text.trim(),
        "email": emailTextEditingController.text.trim(),
        "phone": phoneTextEditingController.text.trim(),
      };
      usersRef.child(firebaseUser.uid).set(userDataMap);
      displayToastMessage(context, 'Your account has been created.');
      Navigator.pushNamedAndRemoveUntil(
          context, HomePage.idPage, (route) => false);
    } else {
      // afficher les message d'erreur
      displayToastMessage(context, 'User account has not been created.');
    }
  }

  displayToastMessage(BuildContext context, String message) {
    Fluttertoast.showToast(msg: message);
  }
}
