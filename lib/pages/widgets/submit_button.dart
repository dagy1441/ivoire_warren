import 'package:flutter/material.dart';

class SubmitButton extends StatelessWidget {
  final String? buttonText;
  const SubmitButton({
    Key? key,
    this.buttonText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: Text(
        buttonText!,
      ),
      style: TextButton.styleFrom(
        primary: Colors.yellow,
        backgroundColor: Colors.amber,
        minimumSize: const Size(350, 50),
        side: const BorderSide(color: Colors.amber, width: 1),
        elevation: 5,
        shadowColor: Colors.yellow,
        textStyle: const TextStyle(
          fontSize: 18,
          fontFamily: 'Brand bold',
        ),
        // padding: const EdgeInsets.symmetric(
        //   vertical: 20,
        //   horizontal: 50,
        // ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24.0),
        ),
      ),
      onPressed: () {
        print('Pressed');
      },
    );
  }
}
