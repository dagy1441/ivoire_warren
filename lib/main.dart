import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:ivoire_warren/pages/home_page.dart';
import 'package:ivoire_warren/pages/login/login_page.dart';

import 'pages/registration/widgets/registration_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

DatabaseReference usersRef = FirebaseDatabase.instance.ref().child("users");

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: "Brand Bold",
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: LoginPage.idPage,
      routes: {
        LoginPage.idPage: (context) => const LoginPage(),
        RegistrationPage.idPage: (context) => RegistrationPage(),
        HomePage.idPage: (context) => const HomePage(),
      },
    );
  }
}
