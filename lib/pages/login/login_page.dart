import 'package:flutter/material.dart';
import 'package:ivoire_warren/pages/registration/widgets/registration_page.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);
  static const String idPage = 'loginPage';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              const SizedBox(height: 35),
              const Image(
                image: AssetImage('assets/images/logo.png'),
                width: 390,
                height: 250,
                alignment: Alignment.center,
              ),
              // SizedBox(height: 15),
              const Text(
                'Login as a Rider',
                style: TextStyle(
                  fontFamily: 'Brand bold',
                  fontSize: 24,
                ),
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    const SizedBox(height: 2.0),
                    const TextField(
                      keyboardType: TextInputType.emailAddress,
                      style: TextStyle(fontSize: 14),
                      decoration: InputDecoration(
                        labelText: 'Email',
                        labelStyle: TextStyle(fontSize: 14),
                        hintStyle: TextStyle(
                          fontSize: 10,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    const SizedBox(height: 2.0),
                    const TextField(
                      obscureText: true,
                      style: TextStyle(fontSize: 14),
                      decoration: InputDecoration(
                        labelText: 'Password',
                        labelStyle: TextStyle(fontSize: 14),
                        hintStyle: TextStyle(
                          fontSize: 10,
                          color: Colors.grey,
                        ),
                      ),
                    ),
                    const SizedBox(height: 12.0),
                    TextButton(
                      child: const Text(
                        'Login',
                      ),
                      style: TextButton.styleFrom(
                        primary: Colors.yellow,
                        backgroundColor: Colors.amber,
                        minimumSize: const Size(350, 50),
                        side: const BorderSide(color: Colors.amber, width: 1),
                        elevation: 5,
                        shadowColor: Colors.yellow,
                        textStyle: const TextStyle(
                          fontSize: 18,
                          fontFamily: 'Brand bold',
                        ),
                        // padding: const EdgeInsets.symmetric(
                        //   vertical: 20,
                        //   horizontal: 50,
                        // ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(24.0),
                        ),
                      ),
                      onPressed: () {
                        print('Pressed');
                      },
                    ),
                  ],
                ),
              ),
              TextButton(
                child: const Text(
                  'Do not have an Account? Register Here.',
                  style: TextStyle(color: Colors.black),
                ),
                onPressed: () {
                  Navigator.pushNamedAndRemoveUntil(
                      context, RegistrationPage.idPage, (route) => false);
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
